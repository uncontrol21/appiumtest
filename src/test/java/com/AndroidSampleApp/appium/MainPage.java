package com.AndroidSampleApp.appium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;



public class MainPage {

    //левое поле ввода
    @FindBy (id = "com.vbanthia.androidsampleapp:id/inputFieldLeft")
    private WebElement inputFieldLeft;
    //правое поле ввода
    @FindBy (id = "com.vbanthia.androidsampleapp:id/inputFieldRight")
    private WebElement inputFieldRight;

    //кнопка очистить все
    @FindBy (id = "com.vbanthia.androidsampleapp:id/resetButton")
    private WebElement resetButton;

    //кнопка деления
    @FindBy (id = "com.vbanthia.androidsampleapp:id/divisionButton")
    private WebElement divisionButton;

    //кнопка умножения
    @FindBy (id = "com.vbanthia.androidsampleapp:id/multiplicationButton")
    private WebElement multiplicationButton;

    //кнопка вычитания
    @FindBy (id = "com.vbanthia.androidsampleapp:id/subtractButton")
    private WebElement subtractButton;

    //кнопка сложения
    @FindBy (id = "com.vbanthia.androidsampleapp:id/additionButton")
    private WebElement additionButton;
    //поле вывода результатов
    @FindBy (id = "com.vbanthia.androidsampleapp:id/resultTextView")
    private WebElement resultTextView;

public MainPage(WebDriver driver)
{
    PageFactory.initElements(driver, this);
}


//Получаем содержимое поля вывода
    public boolean resultTextViewsGetText(String text)
{

    return resultTextView.getText().contains(text);
}

public boolean resultTextViewEmpty(){

    return resultTextView.getText().isEmpty();
}
    //Ввод в левое поле
    public void inputFieldLeftSendText(String number)
    {
        inputFieldLeft.sendKeys(number);
    }
    //Ввод в правое поле
    public void inputFieldRightSendText(String number)
    {
        inputFieldRight.sendKeys(number);
    }
    //Нажатие кнопки деления
    public void divisionButtonClick()
    {
        divisionButton.click();
    }

    //получаем содержимое правого поля ввода
    public String inputFieldRightText()
    {

        return inputFieldRight.getText();
    }

    //получаем содержимое левого поля ввода
    public String inputFieldLeftText()
    {
        return inputFieldLeft.getText();
    }
    //нажатие кнопки сложения
    public void additionButtonClick(){
        additionButton.click();
    }
    //Нажатие кнопки умножения

    public void multiplicationButtonClick(){
       multiplicationButton.click();
    }
    //Нажатие на кнопку вычитания
    public void  subtractButtonClick(){
    subtractButton.click();
    }

    public void resetButtonClick(){
        resetButton.click();
    }
}
