package com.AndroidSampleApp.appium;

import io.appium.java_client.android.AndroidDriver;
import org.junit.jupiter.api.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.remote.DesiredCapabilities;


public class Tests {

    private static AndroidDriver driver;
    private  MainPage mainPage;
    @BeforeAll
    static void setUpAll(){
    }

    @BeforeEach
    public void setUp() throws MalformedURLException{

        URL serverURL = new URL("http://127.0.0.1:4723/wd/hub");

        DesiredCapabilities capabilities = new DesiredCapabilities();

        capabilities.setCapability("deviceName", "pixel 2");
        // ��� �� �� ��������� ����������
        capabilities.setCapability("platformName", "Android");
        // ������ ��
        capabilities.setCapability("platformVersion", "7.1");

        capabilities.setCapability("udid", "emulator-5554");

        String apk_Path = System.getProperty("user.dir") + "/src/main/resources/app-debug.apk";

       capabilities.setCapability("app", apk_Path);
         //capabilities.setCapability("appPackage", "com.vbanthia.androidsampleapp");
        // capabilities.setCapability("appActivity", "com.vbanthia.androidsampleapp.MainActivity");

        driver = new AndroidDriver(serverURL, capabilities);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        mainPage = new MainPage(driver);
    }
    @AfterEach
    public void tearDown() {
        {
            driver.quit();
        }
    }
    // ��������
    @Test
    public void  AdditionOfIntegers() {

        mainPage.inputFieldLeftSendText("13");
        mainPage.inputFieldRightSendText("6");
        mainPage.additionButtonClick();
        Assertions.assertTrue(mainPage.resultTextViewsGetText("= 19.00"));

    }

    @Test
    public void  AdditionIntegersAndFractionalNumbers() {
        mainPage.inputFieldLeftSendText("13.5");
        mainPage.inputFieldRightSendText("6");
        mainPage.additionButtonClick();
        Assertions.assertTrue(mainPage.resultTextViewsGetText("= 19.5"));

    }
    @Test
    public void  AdditionFractionalNumbers() {

        mainPage.inputFieldLeftSendText("190.2");
        mainPage.inputFieldRightSendText("6.1");
        mainPage.additionButtonClick();
        Assertions.assertTrue(mainPage.resultTextViewsGetText("= 196.3"));
    }
    @Test
    public void  AdditionNegativeNumbers() {

        mainPage.inputFieldLeftSendText("-6");
        mainPage.inputFieldRightSendText("-6.1");
        mainPage.additionButtonClick();
        Assertions.assertTrue(mainPage.resultTextViewsGetText("= -12.1"));

    }
    @Test
    public void  AdditionZero() {

        mainPage.inputFieldLeftSendText("-6");
        mainPage.inputFieldRightSendText("0");
        mainPage.additionButtonClick();
        Assertions.assertTrue(mainPage.resultTextViewsGetText("= -6.00"));

    }
    // ���������
    @Test
    public void  multiplicationOfIntegers() {

        mainPage.inputFieldLeftSendText("13");
        mainPage.inputFieldRightSendText("6");
        mainPage.multiplicationButtonClick();
        Assertions.assertTrue(mainPage.resultTextViewsGetText("= 78.00"));

    }

    @Test
    public void  multiplicationIntegersAndFractionalNumbers() {
        mainPage.inputFieldLeftSendText("13.5");
        mainPage.inputFieldRightSendText("6");
        mainPage.multiplicationButtonClick();
        Assertions.assertTrue(mainPage.resultTextViewsGetText("= 81.00"));

    }
    @Test
    public void  multiplicationPositiveByNegative() {

        mainPage.inputFieldLeftSendText("11.2");
        mainPage.inputFieldRightSendText("-4");
        mainPage.multiplicationButtonClick();
        Assertions.assertTrue(mainPage.resultTextViewsGetText("= -44.8"));
    }

    @Test
    public void  multiplicationFractionalNumbers() {

        mainPage.inputFieldLeftSendText("19.2");
        mainPage.inputFieldRightSendText("6.1");
        mainPage.multiplicationButtonClick();
        Assertions.assertTrue(mainPage.resultTextViewsGetText("= 117.12"));
    }
    @Test
    public void  multiplicationNegativeNumbers() {

        mainPage.inputFieldLeftSendText("-5");
        mainPage.inputFieldRightSendText("-4");
        mainPage.multiplicationButtonClick();
        Assertions.assertTrue(mainPage.resultTextViewsGetText("= 20.00"));

    }
    @Test
    public void  multiplicationPositiveByZero() {

        mainPage.inputFieldLeftSendText(ConfProperties.getProperty("random_Positive_number"));
        mainPage.inputFieldRightSendText("0");
        mainPage.multiplicationButtonClick();
        Assertions.assertTrue(mainPage.resultTextViewsGetText("= 0.00"));

    }
    @Test
    public void  multiplicationNegativeByZero() {

        mainPage.inputFieldLeftSendText(ConfProperties.getProperty("random_Negative_number"));
        mainPage.inputFieldRightSendText("0");
        mainPage.multiplicationButtonClick();
        Assertions.assertTrue(mainPage.resultTextViewsGetText("= 0.00"));

    }


    // �������

    @Test
    public void  divisionIntegersNumbers() {

        mainPage.inputFieldLeftSendText("14");
        mainPage.inputFieldRightSendText("6");
        mainPage.divisionButtonClick();
        Assertions.assertTrue(mainPage.resultTextViewsGetText("= 2.33"));

    }
    @Test
    public void  divisionZeroByZero() {

        mainPage.inputFieldLeftSendText("0");
        mainPage.inputFieldRightSendText("0");
        mainPage.divisionButtonClick();
        Assertions.assertTrue(mainPage.resultTextViewsGetText("= NaN"));

    }

    @Test
    public void  divisionIntegersOnFractionalNumbers() {
        mainPage.inputFieldLeftSendText("6");
        mainPage.inputFieldRightSendText("2.5");
        mainPage.divisionButtonClick();
        Assertions.assertTrue(mainPage.resultTextViewsGetText("= 2.4"));

    }

    @Test
    public void  divisionFractionalNumbersOnIntegers() {
        mainPage.inputFieldLeftSendText("13.5");
        mainPage.inputFieldRightSendText("6");
        mainPage.divisionButtonClick();
        Assertions.assertTrue(mainPage.resultTextViewsGetText("= 2.25"));

    }
    @Test
    public void  divisionPositiveByNegative() {

        mainPage.inputFieldLeftSendText("11.2");
        mainPage.inputFieldRightSendText("-4");
        mainPage.divisionButtonClick();
        Assertions.assertTrue(mainPage.resultTextViewsGetText("= -2.8"));
    }

    @Test
    public void  divisionFractionalNumbers() {

        mainPage.inputFieldLeftSendText("19.2");
        mainPage.inputFieldRightSendText("6.1");
        mainPage.divisionButtonClick();
        Assertions.assertTrue(mainPage.resultTextViewsGetText("= 3.15"));
    }
    @Test
    public void  divisionNegativeNumbers() {

        mainPage.inputFieldLeftSendText("-5");
        mainPage.inputFieldRightSendText("-4");
        mainPage.divisionButtonClick();
        Assertions.assertTrue(mainPage.resultTextViewsGetText("= 1.25"));

    }

    @Test
    public void  divisionNegativeByZero() {

        mainPage.inputFieldLeftSendText(ConfProperties.getProperty("random_Negative_number"));
        mainPage.inputFieldRightSendText("0");
        mainPage.divisionButtonClick();
        Assertions.assertTrue(mainPage.resultTextViewsGetText("= Infinity"));

    }


    @Test
    public void processingDivisionPositiveByZero() {

        mainPage.inputFieldLeftSendText(ConfProperties.getProperty("random_Positive_number"));
        mainPage.inputFieldRightSendText("0");
        mainPage.divisionButtonClick();
        Assertions.assertTrue(mainPage.resultTextViewsGetText("= Infinity"));

    }
 // ���������

    @Test
    public void subtractIntegersNumbers() {

        mainPage.inputFieldLeftSendText("5");
        mainPage.inputFieldRightSendText("5");
        mainPage.subtractButtonClick();
        Assertions.assertTrue(mainPage.resultTextViewsGetText("= 0"));

    }

    @Test
    public void  subtractIntegersOnFractionalNumbers() {
        mainPage.inputFieldLeftSendText("6");
        mainPage.inputFieldRightSendText("2.5");
        mainPage.subtractButtonClick();
        Assertions.assertTrue(mainPage.resultTextViewsGetText("= 3.5"));

    }

    @Test
    public void  subtractFractionalNumbersOnIntegers() {
        mainPage.inputFieldLeftSendText("13.5");
        mainPage.inputFieldRightSendText("6");
        mainPage.subtractButtonClick();
        Assertions.assertTrue(mainPage.resultTextViewsGetText("= 7.5"));

    }
    @Test
    public void  subtractPositiveByNegative() {

        mainPage.inputFieldLeftSendText("11.2");
        mainPage.inputFieldRightSendText("-4");
        mainPage.subtractButtonClick();
        Assertions.assertTrue(mainPage.resultTextViewsGetText("= 15.2"));
    }

    @Test
    public void  subtractFractionalNumbers() {

        mainPage.inputFieldLeftSendText("19.2");
        mainPage.inputFieldRightSendText("6.1");
        mainPage.subtractButtonClick();
        Assertions.assertTrue(mainPage.resultTextViewsGetText("= 13.1"));
    }
    @Test
    public void subtractNegativeNumbers() {

        mainPage.inputFieldLeftSendText("-5");
        mainPage.inputFieldRightSendText("-4");
        mainPage.subtractButtonClick();
        Assertions.assertTrue(mainPage.resultTextViewsGetText("= -1.00"));

    }

    @Test
    public void  subtractNegativeByZero() {

        mainPage.inputFieldLeftSendText(ConfProperties.getProperty("random_Negative_number"));
        mainPage.inputFieldRightSendText("0");
        mainPage.subtractButtonClick();
        Assertions.assertTrue(mainPage.resultTextViewsGetText("= Infinity"));

    }
    //other
    @Test
    public void processingEmptyInputLeftFields() {

        mainPage.inputFieldRightSendText(ConfProperties.getProperty("random_Positive_number"));
        mainPage.divisionButtonClick();
        Assertions.assertTrue(mainPage.resultTextViewsGetText("Please, fill the input fields correctly"));

    }
    @Test
    public void processingEmptyInputRightFields() {

        mainPage.inputFieldLeftSendText(ConfProperties.getProperty("random_Positive_number"));
        mainPage.divisionButtonClick();
        Assertions.assertTrue(mainPage.resultTextViewsGetText("Please, fill the input fields correctly"));

    }
    @Test
    public void  BannedCharacterInputProcessing() {

        mainPage.inputFieldLeftSendText(ConfProperties.getProperty("random_Word"));

        mainPage.inputFieldRightSendText(ConfProperties.getProperty("random_Word"));

        Assertions.assertEquals( "",mainPage.inputFieldRightText() );
        Assertions.assertEquals( "", mainPage.inputFieldLeftText() );

    }

    @Test
    public void  AllowedCharacterInputProcessing() {

        mainPage.inputFieldLeftSendText(ConfProperties.getProperty("random_Negative_number"));

        mainPage.inputFieldRightSendText(ConfProperties.getProperty("random_Negative_number"));

        Assertions.assertEquals( ConfProperties.getProperty("random_Negative_number") , mainPage.inputFieldRightText());
        Assertions.assertEquals( ConfProperties.getProperty("random_Negative_number"), mainPage.inputFieldRightText());

    }

    @Test
    public void ResetInputField() {

        mainPage.inputFieldLeftSendText(ConfProperties.getProperty("random_Negative_number"));
        mainPage.inputFieldRightSendText(ConfProperties.getProperty("random_Negative_number"));
        mainPage.resetButtonClick();
        Assertions.assertEquals("" ,mainPage.inputFieldRightText());
        Assertions.assertEquals( "" , mainPage.inputFieldLeftText());

    }
    @Test
    public void ResetOutputField() {

        mainPage.inputFieldLeftSendText(ConfProperties.getProperty("random_Negative_number"));
        mainPage.inputFieldRightSendText(ConfProperties.getProperty("random_Negative_number"));
        mainPage.additionButtonClick();
        mainPage.resetButtonClick();
        Assertions.assertTrue( mainPage.resultTextViewEmpty());

    }





}
